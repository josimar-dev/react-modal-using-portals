import React, { Component } from 'react';
import ReactDOM from "react-dom";
import Styled from "styled-components";
import Modal from "./Modal/Modal";

const ModalContent = Styled.div`
  background-color: rgba(0, 0, 0, 0.5);
  position: fixed;
  height: 100%;
  width: 100%;
  top: 0;
  left: 0;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const ModalView = Styled.div`
  width: 70%;
  height: 70%;
  display: flex;
  flex-direction: column;
  background: #f2f2f2;
  box-shadow: 0 0 60px 10px rgba(0, 0, 0, 0.9);
  padding: 20px 50px 20px 20px;
  overflow-x: scroll;
  position: relative;
`;

const CloseModal = Styled.span`
  position: absolute;
  right: 16px;
  top: 16px;
  cursor: pointer;
  font-size: 22px;
`;

const AppContainer = Styled.div`
  display: flex;
  width: 100%;
  height: 100vh;
  justify-content: center;
  align-items: center;
`;

const OpenModal = Styled.div`
    display: flex;
    padding: 50px;
    justify-content: center;
    align-items: center;
    background: #b2ef54;
    transition: all 0.5s;
    box-shadow: 10px 10px 5px -3px rgba(0,0,0,0.75);
    &:hover {
        opacity: 1;
        transform: scale(1.5, 1.5);
    }
    color: #ffffff;
    font-size: 50px;
    cursor: pointer;

`;

class App extends Component {
    constructor(props) {
      super(props);
      this.state = { showModal: false };
    }

    handleShow = () => {
      this.setState({ showModal: true });
    };

    handleHide = () => {
      this.setState({ showModal: false });
    };

    render() {
      // Show a Modal on click
      // ( In a real app, don't forget to use ARIA attributes for accessibility! )
      const modal = this.state.showModal ? (
        <Modal>
          <ModalContent>
            <ModalView>
              <h1>Modal Example</h1>
              This is being rendered inside the #modal-container div.
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Repudiandae expedita corrupti laudantium aperiam, doloremque
                explicabo ipsum earum dicta saepe delectus totam vitae ipsam
                doloribus et obcaecati facilis eius assumenda, cumque.
              </p>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Repudiandae expedita corrupti laudantium aperiam, doloremque
                explicabo ipsum earum dicta saepe delectus totam vitae ipsam
                doloribus et obcaecati facilis eius assumenda, cumque.
              </p>
              <CloseModal onClick={this.handleHide}>x</CloseModal>
            </ModalView>
          </ModalContent>
        </Modal>
      ) : null;

      return (
        <AppContainer>
          <OpenModal onClick={this.handleShow}>MODAL</OpenModal>
          {modal}
        </AppContainer>
      );
    }
}

export default App;
